﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Pogo : MonoBehaviour
{
	private static float BPS = 104f/60f;
	private static float GLOBAL_SCALE = 0.3f;

	[SerializeField]
	private float scale = 1.05f;

	private void OnEnable() {
		transform.DOScaleY(Mathf.Lerp(1f, scale, GLOBAL_SCALE), 1f/BPS/2).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutBounce);
	}
}
