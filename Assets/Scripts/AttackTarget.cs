﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AttackTarget : MonoBehaviour
{
	[SerializeField]
	private AttackType attackType = AttackType.kick;

	[SerializeField]
	private float spawnBeerProp = 0.25f;
	
	[SerializeField]
	private GameObject beerBottle = null;

	[SerializeField]
	private float beerSpawnDistance = 2f;

	[SerializeField]
	private float beerSpawnOffset = 0.5f;

	[SerializeField]
	private float beerThrowForce = 0.7f;

	[SerializeField]
	private float hitThrowForce = 0.0f;

	private SoundBoard sounds;

	private AudioSource audioSource = null;

	private bool hasBeenHit = false;

	void Start() {
		audioSource = GetComponent<AudioSource>();
		sounds = GetComponent<SoundBoard>();
	}

	public bool wouldHit(AttackType type) {
		return type==attackType && !hasBeenHit;
	}
	public bool attack(AttackType type) {
		if(type!=attackType || hasBeenHit)
			return false;
		
		hasBeenHit = true;
		if(audioSource!=null)
			audioSource.Stop();
		
		if(sounds!=null)
			sounds.play("hit");

		// TODO: play animation
		transform.DORotate(new Vector3(0,0,-90f), 0.3f);
		if(hitThrowForce>0f) {
			float camHalfHeight = Camera.main.orthographicSize;
			float camHalfWidth = camHalfHeight * Screen.width / (float)Screen.height;
			float camXPos = Camera.main.transform.position.x;
			float camYPos = Camera.main.transform.position.y;
			transform.DOJump(transform.position + new Vector3(camXPos+camHalfWidth*2f, camYPos+camHalfHeight/2f, 0), hitThrowForce, 1, 2f)
				.OnComplete(() => Destroy(gameObject, 3f));
		}

		if(Random.value < spawnBeerProp) {
			var beer = Instantiate(beerBottle, transform.position + new Vector3(beerSpawnOffset,0,0), Quaternion.identity);
			beer.transform.DOJump(transform.position + new Vector3(beerSpawnDistance, 0, 0), beerThrowForce, 1, 1f);

			if(sounds!=null)
				sounds.play("beer");
		}
		
		return true;
	}

}
