﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Hover : MonoBehaviour
{
	[SerializeField]
	private float offset = 1f;

	private void OnEnable() {
		transform.DOMoveY(offset, 1f).SetLoops(-1, LoopType.Yoyo);
	}
}
