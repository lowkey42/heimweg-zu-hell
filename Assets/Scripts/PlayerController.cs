﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerController : MonoBehaviour
{

	private Player playerComp;

	void Start() {
		playerComp = GetComponent<Player>();
	}

	void Update()
	{
		playerComp.move(Input.GetAxis("Move"));

		if(Input.GetButtonUp("Kick")) {
			playerComp.queueAttack(AttackType.kick);
		} else if(Input.GetButtonUp("Punch")) {
			playerComp.queueAttack(AttackType.punch);
		} else if(Input.GetButtonUp("Shout")) {
			playerComp.queueAttack(AttackType.shout);
		}

#if (UNITY_STANDALONE) 
		if(Input.GetKeyDown(KeyCode.Escape) == true) {
			Application.Quit();
		}
#endif
	}
}
