﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BGM : MonoBehaviour
{
	[SerializeField] private AudioSource intro;
	[SerializeField] private AudioSource loop;

	private void Awake() {
		double time = AudioSettings.dspTime + 0.3d;
		intro.PlayScheduled(time);
		double duration = (double)intro.clip.samples / intro.clip.frequency;
		loop.PlayScheduled(time + duration);
	}

	public void FadeOut(float duration) {
		Debug.Log("FadeOut");
		var v = loop.DOFade(0, duration);
		v.SetUpdate(true);
		
	}

}
