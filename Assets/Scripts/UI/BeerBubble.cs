﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerBubble : MonoBehaviour {
    public float speed { get; set; }
	public float offSpeed { get; set; }
	public float xCutoff { get; set; }

	[SerializeField] public RectTransform rt;

	// Update is called once per frame
	void Update() {
		if (rt.localPosition.x > xCutoff) {
			Destroy(gameObject);
			return;
		}

		rt.localPosition = new Vector2(rt.localPosition.x + speed * Time.deltaTime, rt.localPosition.y + offSpeed * Time.deltaTime);
	}
}
