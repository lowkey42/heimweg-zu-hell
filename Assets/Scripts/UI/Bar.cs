﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void BarEvent();

public enum BarMode {
	aktivitöt,
	powerpegel,
	progress
}

public class Bar : MonoBehaviour
{
	[SerializeField] private RectTransform maskTransform;
	[SerializeField] private float currentFill = 1.0f;
	[SerializeField] [Range(2,10f)] private float lerpFactor = 5;
	[SerializeField] private BarMode mode;

	private Player player;

	public event BarEvent onFull;
	public event BarEvent onEmpty;

	private float targetVal {
		get {
			switch (mode) {
				case BarMode.aktivitöt:
					return player.GetAktiMeter();
				case BarMode.powerpegel:
					return player.GetBeerMeter();
				case BarMode.progress:
					return player.GetProgress();
				default:
					return 0;
			}
		}
	}

	private void OnEnable() {
		player = FindObjectOfType<Player>();
	}

	private void Update() {
		float f = targetVal;
		currentFill = Mathf.Lerp(currentFill, f, Mathf.Clamp01(lerpFactor * Time.deltaTime));
		maskTransform.localScale = new Vector3(currentFill, 1, 1);
		if (f < 0.0001f) {
			onEmpty?.Invoke();
		} else if (f > 0.999f) {
			onFull?.Invoke();
		}
	}

}
