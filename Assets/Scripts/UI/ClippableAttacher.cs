﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClippableAttacher : MonoBehaviour
{
	[SerializeField] private GameObject target;
	[SerializeField] private RectMask2D mask;

	private void OnEnable() {
		var clippables = target.GetComponentsInChildren<IClippable>();
		Debug.Assert(clippables.Length > 0, "No clippables to add");
		foreach (var c in clippables) {
			mask.AddClippable(c as IClippable);
		}
	}

	private void OnDisable() {
		var clippables = target.GetComponentsInChildren<IClippable>();
		foreach (var c in clippables) {
			mask.RemoveClippable(c as IClippable);
		}
	}
}
