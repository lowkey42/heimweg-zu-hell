﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerBubbleSpawner : MonoBehaviour {

	[SerializeField] private BeerBubble bubblePrefab;
	[SerializeField] private float lowerPosition;
	[SerializeField] private float upperPosition;
	[SerializeField] private float minY;
	[SerializeField] private float maxY;
	[SerializeField] private float minSpeed;
	[SerializeField] private float maxSpeed;
	[SerializeField] private float spawnDelay;
	[SerializeField] private float minScale;
	[SerializeField] private float maxScale;
	[SerializeField] private float minOffspeed;
	[SerializeField] private float maxOffspeed;

	private float progress;

	private void Update() {
		progress += Time.deltaTime;
		if (progress > spawnDelay) {
			progress = 0;
			SpawnBubble();
		}
	}

	private void SpawnBubble() {
		float y = Random.Range(minY, maxY);
		float speed = Random.Range(minSpeed, maxSpeed);
		float offspeed = Random.Range(minOffspeed, maxOffspeed);
		float scale = Random.Range(minScale, maxScale);
		var bubble = Instantiate(bubblePrefab, transform);
		bubble.rt.localPosition = new Vector2(lowerPosition, y);
		bubble.speed = speed;
		bubble.offSpeed = offspeed;
		bubble.xCutoff = upperPosition;
		bubble.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
	}
}
