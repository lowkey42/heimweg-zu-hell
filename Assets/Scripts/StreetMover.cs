﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetMover : MonoBehaviour
{

	[SerializeField] private GameObject[] streetparts;
	[SerializeField] private float speed;
	[SerializeField] private float streetwidth;
	[SerializeField] private float cutoff;
	[SerializeField] private Player player;

	private void Update() {
		float off = ParallaxScroller.SPEED_FACTOR * speed * Time.deltaTime + (player.parallaxOffset * speed);
		for (int i = 0; i < streetparts.Length; ++i) {
			var street = streetparts[i];
			var pos = street.transform.position;
			pos.x += off;
			if (pos.x < cutoff) {
				pos.x += streetparts.Length * streetwidth;
			}
			street.transform.position = pos;
		}
	}
}
